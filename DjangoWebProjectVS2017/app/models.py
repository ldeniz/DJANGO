"""
Definition of models.
"""

from django.db import models
from django.db.models.fields import CharField
from django.db.models.fields import DateField

# Create your models here.
class Question(models.Model):
    question_text = models.CharField(max_length=200)
    question_cat = models.CharField(max_length=200, default="Sin categorizar")
    pub_date = models.DateTimeField('date published')
    question_choices_total = models.IntegerField(default='0')

class Choice(models.Model):
    question = models.ForeignKey(Question)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    correct_answer = models.BooleanField(default=False)

class User(models.Model):
    email = models.CharField(max_length=200)
    nombre = models.CharField(max_length=200)